from django.http import HttpResponse


def index(request):
    return HttpResponse("<h1>CALCULADORA.</h1> <br>"
                        "Introduzca una operacion para poder utilizarla.")

def suma(request, num1, num2):
    result = num1 + num2
    return HttpResponse("<h1>El resultado de la suma entre " + str(num1) + " y " + str(num2)
                        + " es: " + str(result)+"</h1>")


def resta(request, num1, num2):
    result = num1 - num2
    return HttpResponse("<h1>El resultado de la resta entre " + str(num1) + " y " + str(num2)
                        + " es: " + str(result)+"</h1>")


def multiplicacion(request, num1, num2):
    result = num1 * num2
    return HttpResponse("<h1>El resultado de la multiplicacion entre " + str(num1) + " y " + str(num2)
                        + " es: " + str(result)+"</h1>")


def division(request, num1, num2):
    result = num1 / num2
    return HttpResponse("<h1>El resultado de la division entre " + str(num1) + " y " + str(num2)
                        + " es: " + str(result)+"</h1>")
