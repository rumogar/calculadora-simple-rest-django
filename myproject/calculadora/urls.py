from django.urls import path

from . import views

urlpatterns = [
    path('suma/<int:num1>/<int:num2>', views.suma),
    path('resta/<int:num1>/<int:num2>', views.resta),
    path('multiplicacion/<int:num1>/<int:num2>', views.multiplicacion),
    path('division/<int:num1>/<int:num2>', views.division),
    path('', views.index),
]